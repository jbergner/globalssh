# GlobalSSH
---

## A word of caution

This project is designed to circumvent firewall restrictions in corporate networks to expose resources behind a firewall to the public internet.

Of course, this may very well be a violation of numerous IT security policies. Before you use the GlobalSSH-server or any concept from the GlobalSSH-project to expose anything from your corporate network to the server or the public internet, make sure, this is compliant with the rules in your organization!

We are not responsible for any consequences like you creating a security breach, exposing sensible data or getting fired!

## Project history

### Before GlobalSSH

This project started as a company-internal project to allow us to create a temporary access to a device behind a customer's firewall.

Our normal access route were VPN clients in the destination network, but sometimes, these clients failed for various reasons.

In order to debug the VPN clients, we needed access to them and had to use proprietary (which basically means untrusted) tools like TeamViewer to access a customer's laptop, and to jump to our VPN clients from there.

This usually meant, we needed to get clearance on the customer's end to even use TeamViewer or whatever alternative we had at hand, only to provide login credentials to our VPN clients over this untrusted connection.

### The idea

To get around the issue of providing credentials over an untrusted connection, we came up with the idea to use SSH tunnels.

The customer's laptop should create an SSH-reverse-tunnel to our SSH server that we could use to access our destination device.

The tunnel is established using ```google-authenticator``` two-factor-authentication. This way, the customer needs a TAN to create the tunnel, which will expire 4 minutes later.

Therefore, the credentials can be provided to the customer over an untrusted connection (like Skype) or even be entered by the company operator over something like TeamViewer without revealing anything of value, finally setting up a secured SSH tunnel that would allow us to access the target device via a TCP port (like SSH or RDP).

Another advantage is, that the SSH tunnel can be created with native tools from Linux and MacOS machines, while for Windows, the PuTTY-SSH-client is available as portable binary, thus, circumventing the need to install something on a corporate machine.

### Further development

The project was extended in multiple ways. It uses ```SSLH``` to make the SSH server as well as a webserver available on port ```443```, as in some networks, the SSH port (```22```) is blocked.

Furthermore, the ```apache2```-webserver can be used as HTTPs-proxy to access the SSH server.

In this setup, the outgoing traffic is plain HTTPs instead of SSH, which can help to circumvent advanced filtering mechanisms like deep package inspection. On the downside, the SSH client needs an additional tool to access the proxy. (Like ```proxytunnel``` on Linux.)

To make the server usable without a trusted local network connection, where you could expose your remote-forwarded ports to, you can now optionally create a forward-user to use ```ssh -L```.

This user will only be able to log in with an SSH key. However to make athorization of SSH keys more convenient, an SSH CA will be created that can be used to sign public SSH keys of authorized users. If you transfer the CA to your local machine, you can do this right from there.

Additionally, all web resources are now offered via HTTPs and it is possible to obtain SSL certificates from Let's Encrypt.

There are three "web sites". The first one is the only one that may optionally be exposed via HTTP without encryption. (However, not on standard port 80.) It is the web site offering the current TAN for creating a reverse SSH tunnel and a list of ports that are in use (optionally, you can also show the destination, where the SSH tunnel is pointing to) which requires authentication via HTTPs. (You can specify a PAM group. Whoever is a member of this group can access the site.) Via HTTP on an alternative port, authentication is not required, while it is assumed that this port is only accessible from a trusted environment.

Then, there is a "site" simply answering with the next free port from the SSH port forward range, that could be used to set up the tunnel. Lastly, there is a site giving instructions on how to use the web server as a proxy for SSH. This vhost is also the proxy itself.

## How to set up GlobalSSH?

### Considerations

This section will tell you how to use this repository to set up your own GlobalSSH-server.

Please consider, that the original setup assumed, that the SSH-port of the server is internet-facing through an external router, that does a port-forward, while the server resides inside your corporate network and so, the the SSH tunnel endpoints on the server can be exposed to the server's network as it is trusted.

Therefore, the TAN website can also be exposed without authentication via HTTP on an alternative HTTP port.


### In a nutshell

* Make sure, ```ansible``` can be executed. Be it through SSH or locally after checkout.
* In the inventory file, enter the correct IP or domain in the ```ansible_host```-variable.
* Update the defaults in ```roles/globalssh-server/defaults/main.yml``` according to your wishes, provide the changes via ```--extra-vars```-flag when executing ansible or do it in an inventory file. **Especially change the tunnel user's password!**
* Run ansible like this:
```
ansible-playbook -i inventory.yml --extra-vars "{"var1": "value", "var2": "value"...}" setup-playbook.yml
```
* Enjoy your new GlobalSSH-server!

### Actual setup

The repository assumes you have a running Linux server. (Tested with Debian 10/Buster and Ubuntu 18.04 LTS, but any distribution based on the ```apt``` package manager might work.)

It will also be assumed that you have SSH access to the server with the ```root```-user, so ```ansible``` can do its job. If you use another user, you can explicitly provide it as variable ```ansible_user```, assuming it can use ```sudo``` to execute commands.
Alternatively, ```ansible``` can be set up to run on the local machine without the need to SSH into the destination, if you have other means of executing shell commands.

Please update your inventory-file, so ansible it can find your destination server and to include the defaults you need.

#### Variables and defaults

Before you deploy with ansible, have a look at ```roles/globalssh-server/defaults/main.yml```! You will see the defaults for your GlobalSSH-server. You might want to adjust them or override them in your ansible-call with the ```--extra-vars```-directive or a specific inventory file.

Here is the explanation of what they do:

* ```globalssh_hostname```: This will be the server's hostname
* ```globalssh_tunnel_user```: The name of the PAM user that will be created for reverse-tunneling.
* ```globalssh_tunnel_password_hash```: Specify the password hash for the tunnel user. On Linux, you can create it with ```mkpasswd --method=sha-512``` (See section "Tunnel password".) **You should really change this.**
* ```globalssh_forward```: Whether a user for local forwarding shall be created. Defaults to false. **You might want to change this.**
* ```globalssh_forward_user```: The name of the PAM user that will be created for forward-tunneling.
* ```globalssh_forward_password_hash```: The password hash for the forward user. Defaults to the same as the tunnel user. **You might want to change this.** Notice, that this password will usually not be required in every-day-usage, as the forward user will be set up for SSH-Key-CA-usage.
* ```globalssh_forward_ca_path```: Where to store the SSH-CA. (Defaults to ```/root/.ssh```.)
* ```globalssh_forward_ca_passphrase```: Passphrase that will be used for SSH-CA. Defaults to no passphrase at all. For now, this must be specified in plain-text. **You might want to change this.** Notice that you should not write your CA's passphrase to an inventory file for security reasons. Provide it via command-line using ```--extra-vars```.
* ```globalssh_api_private_port```: Private port used for API. Defaults to ```8081```.
* ```globalssh_ports_min```: Lowest port of forwarding range that will be opened in the firewall.
* ```globalssh_ports_max```: Highest port of forwarding range that will be opened in the firewall.
* ```globalssh_show_origins```: Whether the origin of an SSH reverse tunnel shall be presented on the TAN-Website. Defaults to ```False```. **You might want to change this.**
* ```globalssh_tan_trusted_port```: Port on which TAN shall be served without authentication and without encryption. Defaults to ```8080```. Notice, this ```cannot``` be port ```80```.
* ```globalssh_open_tan_trusted_port```: Whether to open the TAN trusted port in machine firewall. Defaults to ```False```.
* ```globalssh_tan_authorized_group```: The group that shall be allowed to access the TAN-site. Defaults to ```tanauthorized```. **You might want to change this.** Notice, that this group will **not** be created by ansible unless specified in ```globalssh_create_tan_authorized_group```.
* ```globalssh_create_tan_authorized_group```: Whether to create the "tan_authorized_group" as unix system group. Defaults to ```False```. You might want to change this.
* ```ga_secret_file```: File to store the google-authenticator-secret in. (Normally, keep this the way it is.)
* ```net_main_if```: Network interface to be used. (Defaults to the default IPv4 uplink.)
* ```sslh_https_private_port```: The port you want to use internally for HTTPs. Don't use ```443``` at this is the **external** port and make sure, it is not in the port forward range!
* ```api_name```: Name of the virtual host, used for next-free-port-API.
* ```letsencrypt_certs```: Whether it shall be attempted to obtain SSL certificates from Let's Encrypt. Defults to ```True```. **You should change this, if you use a domain that cannot be used reliably with Let's Encrypt.** (Like ```amazonaws.com``` and ```xip.io```.
* ```proxy_name```: Name of the virtual host, used for the apache2-SSH-proxy.
* ```proxy_machine_fqdn```: Domain, where you can reach the server. **You should change this.**
* ```api_fqdn```: This creates the apache2-vhost-servername for the API from ```api_name``` and ```proxy_machine_fqdn```. **Keep this the way it is!**
* ```proxy_fqdn```: This creates the apache2-vhost-servername for the SSH proxy from ```proxy_name``` and ```proxy_machine_fqdn```. **Keep this the way it is!**
* ```proxy_site_file```: Name of the apache2-config-file for the SSH-proxy. Please note, that due to a bug in apache2, the site name must be the alphanumerically first file in apache's ```sites-enabled```-directory. **Change this only if really needed!**

#### Tunnel password

To create a reverse SSH tunnel, you need the proper username and google-authenticator-TAN, but also still a **static password**.

For this password, choose something simple and keep in mind, that everybody who uses your server needs to know this password. It is more like a technical necessity than a real security feature.

However, suppose your password shall be "```secret```".

You should then create the password hash with
```
mkpasswd --method=sha-512
```
and enter "```secret```" afterwards. The resulting salted hash must be the value for the ```globalssh_tunnel_password_hash```-variable.

After setting up the server, a reverse tunnel can be created with the user specified in ```globalssh_tunnel_user``` ("```tunnel```" by default), the password "```secret```" and the google-authenticator-TAN that changes every 30 seconds. (However, the TAN is valid for around 4 minutes, but only once.)

#### Deployment with oneliner

Finally, you can deploy with a command like this:
```
ansible-playbook -i inventory.yml --extra-vars "{"var1": "value", "var2": "value"...}" setup-playbook.yml
```
Notice, that a boolean value like ```True``` or ```False``` must not be set in quotes! This is especially important for the parameter ```globalssh_show_origins```

#### Alternative: Deployment with inventory-file

For larger environments or if you call this role automatically (may be using [Terraform](https://www.terraform.io/), it is suggested that you create a full inventory file in yml-format. That way, you store the settings that you use to activate the Ansible role which might be helpful for debugging or re-executing.

An example for executing Ansible on a remote host is provided as ```inventory.yml```. To execute Ansible locally, have a look at ```inventory-localhost.yml```

### Deploy on AWS

The repository has been restructured to work with an AWS EC2 instance. Just change your ansible call to use become (```-b```-flag) and provide the following extra variables:
```
ansible-playbook -b -i inventory.yml --extra-vars "{"ansible_host": "YOUR_AWS_IP", "ansible_user": "AWS_USER" ...}" setup-playbook.yml
```
