# Open Points

There are some open points in this project that will be tracked, here.

* Provide license information.
* Provide user-documentation (may be in HTML) about how to use this server. Especially have a look at ProxyTunnel with PuTTY on Windows: https://medium.com/@shrimpy/how-to-config-putty-ssh-over-to-proxy-tunnel-5dd72f18bc77
* Sign HostKey.
* Automatically obtain Let'sEncrypt-Certificate. --> Test on machine with proper domain pending.
* Clean up repo.
* Update README.
