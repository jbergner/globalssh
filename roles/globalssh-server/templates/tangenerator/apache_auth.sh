#!/bin/bash

if ! id -u "$USER"
then
	echo "User does not exist."
	exit 1
fi

if ! id -nG "$USER" | grep -qw "$1"
then
	echo "User not in authorized group."

	exit 1
fi



echo -e "$USER\n$PASS" | pwauth
if [ "$?" == "0" ]
then
	exit 0
else
	echo "Password incorrect."

	exit 1
fi
