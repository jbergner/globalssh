#!/bin/bash

while [ 0 ]
do
	/bin/echo "<b>Verification code for \"{{ globalssh_tunnel_user }}@{{ globalssh_hostname }}\": $(oathtool --totp --base32 {{ ga_secret.stdout }} -d 6)</b> <br> <br> If this code does not work, please try to reload this website! <br> <br> <b>Ports that are currently in use: </b> <br> $(lsof -i -n | egrep "\<sshd\>" | grep {{ globalssh_tunnel_user }} | grep LISTEN | cut -d ":" -f 2 | cut -d " " -f 1 | awk '!seen[$0]++' | sort | tr '\n' '%' | sed "s/%/<br>/g")" > /var/www/html/index.html
	sleep 2
done
