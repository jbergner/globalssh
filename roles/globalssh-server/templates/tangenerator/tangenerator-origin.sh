#!/bin/bash

while [ 0 ]
do
	/bin/echo "<b>Verification code for \"{{ globalssh_tunnel_user }}@{{ globalssh_hostname }}\": $(oathtool --totp --base32 {{ ga_secret.stdout }} -d 6)</b> <br> <br> If this code does not work, please try to reload this website! <br> <br> <b>Ports that are currently in use and origin: </b> <br>" > /var/www/html/index.html
	for pid in $(lsof -i -n | egrep "\<sshd\>" | grep {{ globalssh_tunnel_user }} | grep LISTEN | awk '{print $9,$2}'  | awk '!seen[$0]++' | awk '{print $NF}' | tr '\n' ' ')
	do
		port="$(lsof -i -n | egrep "\<sshd\>" | grep {{ globalssh_tunnel_user }} | grep LISTEN | awk -v var="$pid" '$2 == var' | cut -d ":" -f 2 | awk '!seen[0]++' | cut -d " " -f 1)"
		origin="$(lsof -i -n | egrep "\<sshd\>" | grep {{ globalssh_tunnel_user }} | grep ESTABLISHED | awk -v var="$pid" '$2 == var' | sed 's/.*->\([^ ]*\) .*/\1/g')"
		/bin/echo "$port ==> $origin <br>" >> /var/www/html/index.html
	done
	sleep 2
done
