from flask import request, Response
import os,subprocess,sys

def main():

    min_port={{ globalssh_ports_min }}
    max_port={{ globalssh_ports_max }}

    cmd="lsof -i -n | egrep \"\\<sshd\\>\" | grep {{ globalssh_tunnel_user }} | grep LISTEN | cut -d \":\" -f 2 | cut -d \" \" -f 1 | awk '!seen[$0]++' | awk '!/^$/' | sort"
    lsof_result = subprocess.check_output(cmd,shell=True).splitlines()
    result=""
    for i in range(min_port,max_port+1):
        cont = 0
        for p in lsof_result:
            if int(p) == i:
                cont = 1
        if cont == 0:
            return str(i)

    return "WARNING. All availabe ports (" + str(min_port)+ " to " + str(max_port) + ") seem to be in use."

if __name__== "__main__":
    from flask import Flask
    app = Flask(__name__)
    app.add_url_rule("/", "main", main)
    app.run(port={{ globalssh_api_private_port }})

