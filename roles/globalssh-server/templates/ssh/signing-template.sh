#!/bin/bash

usage(){
	echo "Usage: $0 key-id sshkey.pub"
	echo ""
	echo "You need to provide exactly two parameters, where the first one"
	echo "is the key-id for the certificate, you want to create."
	echo "Think of this key-id as a certificate CommonName (CN) or simply"
	echo "as a unique identifier."
	echo "The key-id is needed if you want to revoke this certificate."
	echo ""
	echo "The second parameter is the SSH-publickey of the user you want"
	echo "to authorize."
	echo ""
	echo "As result, a cert-file will be created. The name of this file"
	echo "will be derived from the public key's file name."
	echo "It must be transfered to the user's SSH-directory, where the"
	echo "private key is located."
}

if [ $# -ne 2 ]
then
	usage
	exit 1
fi

key_id="$1"
pubkey="$2"

if [ ! -f "$pubkey" ]
then
	usage
	exit 1
fi

ssh-keygen \
	-s "{{ globalssh_forward_ca_path }}/{{ globalssh_forward_user }}_ca" \
	-I "$key_id" \
	-n "{{ globalssh_forward_user }}" \
	"$pubkey"

if [ "$?" == "0" ]
then
	echo "Successfully created user-certificate \"${pubkey%.pub}-cert.pub\"."
	echo "This has to be transferred to the user's private key's location."
	exit 0
else
	echo "Something went wrong. Aborting."
	exit 1
fi
	
